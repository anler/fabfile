from fabric.api import env, run, task, prompt, local, put, cd
from fabric.colors import red, green
from fabric.utils import puts
from fabric.state import output
from fabric.contrib.files import exists



@task(alias='git-archive')
def git_archive(project_name, revision='HEAD'):
    """Creates a gzipped tar file containing the tree structure for the named
    revision
    """
    import gzip

    filename = '%s-%s.tar.gz' % (project_name, git_rev_id(revision))
    command = 'git archive --format=tar %s' % revision
    archive = local(command, capture=True)
    if archive.succeeded:
        gzipped = gzip.open(filename, 'wb')
        gzipped.write(archive)
        gzipped.close()
        print(green('Created file %s' % filename))


@task(alias="git-rev-parse")
def git_rev_parse(revision='HEAD'):
    """Get ID of the given revision
    """
    revision_id = local("git rev-parse %s" % revision, capture=True)
    print(green("Revision ID: %s" % revision_id))
    return revision_id
